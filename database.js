'use strict';

const _ = require('lodash');
const Sequelize = require('sequelize');
const moment = require('moment-timezone');
const config = {
  mysql: {
    replicas_quantity: process.env.MYSQL_REPLICAS_QUANTITY ? Number(process.env.MYSQL_REPLICAS_QUANTITY) : 1,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    port: process.env.MYSQL_PORT,
    write: {
      host: process.env.MYSQL_WRITE_HOST,
      pool: { min: process.env.MYSQL_WRITE_POOL_MIN, max: process.env.MYSQL_WRITE_POOL_MAX, idle: process.env.MYSQL_WRITE_POOL_IDLE }
    },
    read: [
      { host: process.env.MYSQL_READ_HOST_1, pool: { min: process.env.MYSQL_READ_POOL_MIN_1, max: process.env.MYSQL_READ_POOL_MAX_1, idle: process.env.MYSQL_READ_POOL_IDLE_1 } }
    ]
  }
};

const createDBConnection = () => {
  const timezone = moment().tz("America/Sao_Paulo").format('Z');
  let configSequelize = {
    dialect: 'mysql',
    port: config.mysql.port,
    dialectOptions: {
      multipleStatements: true
    },
    logging: console.log,
    replication: {},
    timezone: timezone
  };
  configSequelize.replication.write = _.isEmpty(config.mysql.write) ? undefined : config.mysql.write;
  let replicas = [];
  let readAux = !_.isEmpty(config.mysql.read) ? config.mysql.read[0] : undefined;
  if (readAux) {
    replicas.push(readAux);
  }
  configSequelize.replication.read = replicas;
  return new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, configSequelize);
};

const sequelize = createDBConnection();

sequelize.authenticate().then((err) => {
  if (err) {}
});

exports.dbConnection = () => {
  if (!sequelize) {
    sequelize = createDBConnection();
  }
  return {
    query: (sql, parameters, callback) => {
      sequelize.query(sql, { replacements: parameters, type: sequelize.QueryTypes.SELECT, logging: false })
        .then((rows) => {
          callback(null, rows);
          return null;
        })
        .catch((error) => {
          callback(error);
          return null;
        });
    }
  };
};

exports.nativeQuery = (sql, parameters) => {
  return new Promise((resolve, reject) => {
    sequelize.query(sql, { replacements: parameters, type: sequelize.QueryTypes.SELECT, logging: false })
      .then((rows) => {
        return resolve(rows);
      }).catch((error) => {
        return reject(error);
      });
  });
}

exports.insert = (sql, parameters) => {
  return new Promise((resolve, reject) => {
    sequelize.query(sql, { replacements: parameters, type: sequelize.QueryTypes.INSERT, logging: false })
      .then((rows) => {
        return resolve(rows);
      }).catch((error) => {
        return reject(error);
      });
  });
}

exports.delete = (sql, parameters) => {
  return new Promise((resolve, reject) => {
    sequelize.query(sql, { replacements: parameters, type: sequelize.QueryTypes.DELETE, logging: true })
      .then((rows) => {
        return resolve(rows);
      }).catch((error) => {
        return reject(error);
      });
  });
}

exports.sequelize = () => {
  if (!sequelize) {
    sequelize = createDBConnection();
  }
  return sequelize;
}

exports.transaction = (sql, callback) => {
  if (!sequelize) {
    sequelize = createDBConnection();
  }
  sequelize.query(sql).spread((results, metadata) => {
    callback(results, metadata);
  });
}