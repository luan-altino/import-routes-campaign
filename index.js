'use strict'

const fs = require('fs');
let initEnvVariables = () => {
  process.env.NODE_ENV = process.env.NODE_ENV || 'development';
  try {
    const envPath = './env';
    fs.statSync(envPath);
    require('dotenv').config({ path: envPath });
  } catch (err) {
    console.log(err);
  }
};
initEnvVariables();
const _ = require('lodash');
const connectionFactory = require('./database');
const xlsx = require('node-xlsx');
const fileName = process.env.FILE;
const modalityId = Number(process.env.MODALITY_ID || 0);

if (!fileName) {
  throw new Error('process.env.FILE is required');
}

if (!modalityId) {
  throw new Error('process.env.MODALITY_ID is required');
}

const insertFromList = () => {
  const workSheetsFromFile = xlsx.parse(fileName);

  let registers = [];
  for (let plan of workSheetsFromFile) {
    let firstLine = true;
    for (let data of plan.data) {
      if (firstLine) {
        firstLine = false;
        continue;
      }

      let zipCodeInitial = data[4];
      let zipCodeFinal = data[5];
      if (zipCodeInitial && zipCodeFinal) {
        registers.push({ zipCodeInitial: zipCodeInitial, zipCodeFinal: zipCodeFinal });
      }
    }
  }

  let sqlInsert = `insert into routes_campaign_carga(modality_id, zipcode_initial, zipcode_final, created_at) values `;
  let count = 0;
  for (let row of registers) {
    count++;
    console.log(row);
    if (count >= registers.length) {
      sqlInsert += ` (${modalityId}, ${row.zipCodeInitial}, ${row.zipCodeFinal}, now());`;
    } else {
      sqlInsert += ` (${modalityId}, ${row.zipCodeInitial}, ${row.zipCodeFinal}, now()),`;
    }
  }

  let sqlDelete = `delete from routes_campaign_carga where id > 0`;
  connectionFactory.delete(sqlDelete, []).then((result) => {
    connectionFactory.insert(sqlInsert, []).then((result) => {
      console.log('inserido com sucesso');
      process.exit(1);
    }).catch((error) => {
      console.log(error);
      process.exit(1);
    });
  }).catch((error) => {
    console.log(error);
    process.exit(1);
  });
}

insertFromList();