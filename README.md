Projeto em nodejs construído para importar asa faixas de cep das campanhas de prazo 

Para o projeto funcionar é necessário ter o arquivo env com as varíaveis de ambiente configuradas, ex:

MYSQL_USER=
MYSQL_PASSWORD=
MYSQL_DATABASE=
MYSQL_PORT=
MYSQL_WRITE_HOST=
MYSQL_WRITE_POOL_MIN=
MYSQL_WRITE_POOL_MAX=
MYSQL_WRITE_POOL_IDLE=
MYSQL_READ_HOST_1=
MYSQL_READ_POOL_MIN_1=
MYSQL_READ_POOL_MAX_1=
MYSQL_READ_POOL_IDLE_1=

Executando o projeto, setar o diretório do arquivo e a modalidade
FILE="/home/PROMOECOMM CD300 26.03.xlsx" MODALITY_ID=5 node index.js